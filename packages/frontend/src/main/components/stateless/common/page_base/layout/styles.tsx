import styled from "@emotion/styled"
import { Header } from "../header"
import { Views } from "../../../views"
import { grid_areas as area_names } from "./values"
import { breakpoint } from "../../../../../utilities/styling/media_query"
import { grid_area } from "../../../../../utilities/styling/grid"

export const Root = styled(Views.Basic)`
	min-height: 100vh;
	display: grid;
	column-gap: 1rem;
	row-gap: 1rem;

	grid:
		"${grid_area(area_names.header, 12)}" 3rem
		"${grid_area(area_names.side.left, 12)}" 5rem
		"${grid_area(area_names.content, 12)}" 1fr
		"${grid_area(area_names.side.right, 12)}" 3rem
		"${grid_area(area_names.footer, 12)}" 3rem
		/ ${grid_area("1fr", 12)};
	${({ theme }) => `
		${breakpoint(theme.breakpoints.small)`
			grid:
				"${grid_area(area_names.header, 12)}" 3rem
				"
					${grid_area(area_names.side.left, 3)}
					${grid_area(area_names.content, 9)}
				" 1fr
				"${grid_area(area_names.side.right, 12)}" 5rem
				"${grid_area(area_names.footer, 12)}" 3rem
				/ ${grid_area("1fr", 12)};
		`}
		${breakpoint(theme.breakpoints.medium)`
			grid:
				"${grid_area(area_names.header, 12)}" 3rem
				"
					${grid_area(area_names.side.left, 3)}
					${grid_area(area_names.content, 7)}
					${grid_area(area_names.side.right, 2)}
				" 1fr
				"${grid_area(area_names.footer, 12)}" 3rem
				/ ${grid_area("1fr", 12)};
		`}
	`}
`

export const areas = {
	Header: styled(Header)`
		grid-area: ${area_names.header};
	`,
	Content: styled(Views.Basic)`
		grid-area: ${area_names.content};
		background-color: lightblue;
	`,
	sides: {
		Left: styled(Views.Basic)`
			grid-area: ${area_names.side.left};
			background-color: blue;
			transition: transform 0.25s ease-in-out;
			&:hover {
				transform: rotate3d(0, 1, 0.25, 0.1turn);
			}
		`,
		Right: styled(Views.Basic)`
			grid-area: ${area_names.side.right};
			background-color: blue;
		`,
	},
	Footer: styled(Views.Basic)`
		background-color: grey;
		grid-area: ${area_names.footer};
	`,
}
