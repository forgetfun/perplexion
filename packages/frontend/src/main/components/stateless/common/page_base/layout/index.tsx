import "./index.styl"

import React from "react"
import Head from "next/head"
import { ThemeProvider } from "@emotion/react"
import lightTheme from "../../../../../theming/themes/light"
import { Root, areas } from "./styles"

export const Layout: React.FC = ({ children }) => (
	<>
		<Head>
			<meta name="viewport" content="width=device-width, initial-scale=1" />
		</Head>
		<ThemeProvider theme={lightTheme}>
			<Root>
				<areas.Header />
				<areas.Content>{children}</areas.Content>
				<areas.sides.Left>Left menu</areas.sides.Left>
				<areas.sides.Right />
				<areas.Footer>Footer</areas.Footer>
			</Root>
		</ThemeProvider>
	</>
)
