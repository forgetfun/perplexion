export class Breakpoint {
	constructor(private readonly point: string) {}

	toMediaQueryCondition() {
		return `min-width: ${this.point}`
	}
}

export default interface Theme {
	breakpoints: Record<"small"| "medium", Breakpoint>
}
