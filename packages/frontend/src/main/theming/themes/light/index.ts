import Theme, { Breakpoint } from "../../theme"

const theme: Theme = {
	breakpoints: {
		small: new Breakpoint("768px"),
		medium: new Breakpoint("980px"),
	},
}

export default theme
