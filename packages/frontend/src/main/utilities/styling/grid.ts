import { repeat } from "../string"

export const grid_area = (name: string, span = 1) => repeat(name, span, " ")
