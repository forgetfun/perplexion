import { Breakpoint } from "../../theming/theme"

export const breakpoint = (bp: Breakpoint) => (
	template: TemplateStringsArray,
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	...strings: any[]
) => (
	`@media (${bp.toMediaQueryCondition()}) {${String.raw(template, ...strings)}}`
)
