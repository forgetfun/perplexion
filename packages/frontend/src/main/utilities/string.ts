
export const repeat = (str: string, count: number, separator?: string) => (
	count === 0
		? ""
		: count === 1
			? str
			: separator === undefined
				? str.repeat(count)
				: Array.from({ length: count }, () => str).join(separator) // Not optimal regarding performance but sufficient for most use cases
)
